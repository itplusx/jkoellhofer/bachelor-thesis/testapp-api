<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/addresses', [
    'uses' => 'AddressController@create'
]);

Route::get('/addresses', [
    'uses' => 'AddressController@getAll'
]);

Route::get('/addresses/{id}', [
    'uses' => 'AddressController@get'
]);

Route::put('/addresses/{id}', [
    'uses' => 'AddressController@update'
]);

Route::delete('/addresses/{id}', [
    'uses' => 'AddressController@delete'
]);