<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function create(Request $request)
    {
        $address = new Address();
        $address->firstName = $request->input('firstName');
        $address->lastName = $request->input('lastName');
        $address->addressLine = $request->input('addressLine');
        $address->city = $request->input('city');
        $address->postCode = $request->input('postCode');
        $address->state = $request->input('state');
        $address->save();
        return response()->json(['address' => $address], 201);
    }

    public function getAll()
    {
        $addresses = Address::all();
        $response = [
            'addresses' => $addresses,
        ];
        return response()->json($response, 200);
    }

    public function get($id)
    {
        $address = Address::find($id);
        return response()->json(['address' => $address], 200);
    }

    public function update(Request $request, $id)
    {
        $address = Address::find($id);
        if (!$address) {
            return response()->json(['message' => 'Document not found'], 404);
        }
        $address->firstName = $request->input('firstName');
        $address->lastName = $request->input('lastName');
        $address->addressLine = $request->input('addressLine');
        $address->city = $request->input('city');
        $address->postCode = $request->input('postCode');
        $address->state = $request->input('state');
        $address->save();
        return response()->json(['address' => $address], 200);
    }

    public function delete($id)
    {
        /** @var Address $address */
        $address = Address::find($id);

        try {
            $status = $address->delete();

            if ($status === true) {
                return response()->json(['message' => 'Address deleted.']);
            } else {
                return response()->json(['error' => 'Could not delete address.'], 500);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}