# Starten des RESTful Webservice mit Laravel

## 1. Vorbereitung

### Installation der Homestead-Umgebung

1. VirtualBox installieren
2. Vagrant installieren
3. laravel/homestead -box zu Vagrant hinzufügen:

   ```vagrant box add laravel/homestead```

4. Homestead von github klonen und installieren:

   ```cd ~```

   ```git clone https://github.com/laravel/homestead.git Homestead```

5. init.sh ausführen, um Homestead.yaml zu erstellen:

   ```sh init.sh```

### Homestead.yaml konfigurieren

1. Mapping der synchronisierten Ordner konfigurieren.

   Bsp.:

   ```folders:```

   ```- map: ~/projects/synced```

   ```to: /home/vagrant/synced```

2. Nginx-Site hinzufügen:

   ```sites:```

   ```- map: testapp-api.dev```

   ```to: /home/vagrant/synced/testapp-api/public```